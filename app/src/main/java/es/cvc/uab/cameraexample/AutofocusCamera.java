package es.cvc.uab.cameraexample;

import android.hardware.Camera;
import android.view.SurfaceHolder;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by carlos on 7/07/15.
 */
public class AutofocusCamera implements Camera.AutoFocusCallback {
    private Camera camera;
    private boolean autofocus;
    private boolean startedPreview;


    public static int REFOCUSING_DELAY = 2500;
    public Timer focusTimer;
    public static boolean refocusingActive = false;


    public AutofocusCamera(Camera camera) {
        this.camera = camera;
        this.autofocus = false;
        this.startedPreview = false;

    }

    public void enableAutofocus () {
        this.autofocus = true;
    }
    public void disableAutofocus() {
        if (!startedPreview) {
            this.autofocus = false;
        }
    }


    public Camera.Parameters getParameters() {
        return camera.getParameters();
    }

    public void setParameters(Camera.Parameters parameters) {
        this.camera.setParameters(parameters);
    }

    public void stopPreview() {
        this.camera.stopPreview();
        if (this.autofocus) {
            startFocusing();
        }

        this.startedPreview = false;
    }

    public void startPreview() {
        this.camera.startPreview();
        if (this.autofocus) {
            stopFocusing();
        }

        this.startedPreview = true;
    }

    public void setPreviewDisplay(SurfaceHolder previewDisplay) throws IOException {
      this.camera.setPreviewDisplay(previewDisplay);
    }

    public void setPreviewCallback(Preview previewCallback) {
        this.camera.setPreviewCallback(previewCallback);
    }

    public void release() {
        this.camera.release();
    }


    /* auto focusing immplementation */

    public void startFocusing(){

        if (refocusingActive){
            return;
        }
        refocusingActive = true;

        focusTimer = new Timer();
        focusTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                if (camera != null) {
                    try {
                        camera.autoFocus(AutofocusCamera.this);
                    } catch (Exception e) {

                    }
                }

            }
        }, 500, REFOCUSING_DELAY);
    }

    public void stopFocusing(){

        camera.cancelAutoFocus();
        if (!refocusingActive){
            return;
        }

        if (focusTimer != null){
            focusTimer.cancel();
            focusTimer.purge();
        }

        refocusingActive = false;

    }


    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        //TODO Applying autofocus

    }


    public void takePicture( Camera.ShutterCallback shutter, Camera.PictureCallback raw, Camera.PictureCallback postview, Camera.PictureCallback jpeg) {
        camera.takePicture(shutter,raw,postview,jpeg);
    }
}
