package es.cvc.uab.cameraexample;

import android.app.Activity;
import android.hardware.Camera;

import java.io.FileOutputStream;

import es.cvc.uab.cameraexample.utils.Utils;

/**
 * Created by carlos on 7/07/15.
 */
public class JpegSaver implements Camera.PictureCallback {
    private final String TAG = "JpegSaver";
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        //Here, we chose internal storage
        try {
            FileOutputStream out = new FileOutputStream("/sdcard/picture.jpeg");
            out.write(data);
            out.flush();
            out.close();
        } catch (Exception e) {
            Utils.PrintException(e);
        }
        camera.startPreview();
    }
}
