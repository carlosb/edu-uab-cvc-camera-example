package es.cvc.uab.cameraexample;


import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

/**
 * Responsible class to implement notifications in the application.
 *
 *
 * @author carlosb
 *
 */
public final class Notification {

	/*
	 * Singleton pattern info
	 */

    private static Notification notification = null;

    public static synchronized Notification getInstance() {
          Notification.notification = new Notification();
            return Notification.notification;
    }

    /**
     * Throws a new toast Dialog.
     */
    public final void newToast(String msg, Context context) {
        final Activity activity = (Activity) context;
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }
}