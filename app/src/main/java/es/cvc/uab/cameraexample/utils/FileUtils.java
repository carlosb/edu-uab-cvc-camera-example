package es.cvc.uab.cameraexample.utils;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import es.cvc.uab.cameraexample.MainActivity;

/**
 * Created by carlos on 7/07/15.
 */
public class FileUtils {
    private final String TAG = "FileUtils";
    public InputStream GetFileInputStream(String filename, Activity activity) throws FileNotFoundException {
            InputStream inputStream = null;
            AssetManager assetManager = activity.getResources().getAssets();
            try {
                inputStream = assetManager.open(filename);
            } catch (IOException e) {
                Log.w(TAG, "It is not possible read the configuration from assets folder");
                String path = Environment.getExternalStorageDirectory()
                        .getAbsolutePath() + File.separator + filename;
                Log.i(TAG, "Trying to read configuration from : " + path);
                inputStream = new FileInputStream(path);
            }
        return inputStream;
    }

    public static Bitmap BytesToBitmap(byte [] bytes, int width, int height) {
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        bmp.copyPixelsFromBuffer(buffer);
        return bmp;
    }

    public static String SetUpStringCalendar() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HHmm");
        return format.format(Calendar.getInstance().getTime());
    }

}

