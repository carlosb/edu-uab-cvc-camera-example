#include "core.h"
#include "logger.h"
#define TAG "EDU_UAB_CVC/core"
namespace cvc {
	namespace core {	
		core::Response Tracker::process(signed char  bytes[], int width, int height)   {
			cv::Mat origBGR;
			cvc::core::YUVarrayToBGRMat(&bytes,origBGR,width,height);
			LOGI(TAG,"Size: width=%d, height=%d",origBGR.cols,origBGR.rows);

			cv::medianBlur(origBGR,this->processResult,15);
			BGRToRGBAMat(this->processResult,this->processResult);
			return Response(true,""
					,this->processResult.cols,this->processResult.rows
					,this->processResult.channels(), (signed char*) this->processResult.data);
		}
	};


};
