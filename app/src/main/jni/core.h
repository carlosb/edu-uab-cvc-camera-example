#ifndef EDU_UAB_CVC_CORE
#define EDU_UAB_CVC_CORE
#include <cstring>
#include <vector>
#include <exception>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/features2d/features2d.hpp>
#include <sstream>
#include <memory>
#include <iostream>
namespace cvc {
	namespace core {

		template<class T> std::string  to_string(T elem)  {
			std::ostringstream stm;
			stm << elem;
			return stm.str();
		}

		inline void YUVarrayToBGRMat(signed char **data, cv::Mat & output, int width, int height) {
			cv::Mat yuvImage(height + height / 2, width, CV_8UC1,*data);
			cv::cvtColor(yuvImage,output,cv::COLOR_YUV420sp2BGR);
		}
		inline void  BGRToRGBAMat(cv::Mat & imgBGR, cv::Mat & output) {
			std::vector<cv::Mat> channels;
			cv::split(imgBGR,channels);
			cv::Mat alpha(imgBGR.rows,imgBGR.cols,CV_8UC1, cv::Scalar(0xff));
			//ARGB
			cv::Mat inMerge[] =  {channels[2], channels[1], channels[0], alpha};
			cv::merge(inMerge,4,output);
		}



		struct Response {
				bool isCorrect;
				std::string message;
				int width;
				int height;
				int channels;
				int resultBytesLength;
				const signed char* resultBytes;


				Response( bool isCorrect_ = true,
						std::string message_ = std::string(),
						int width_ = 0,
						int height_ = 0,
						int channels_ = 0,
						signed char* resultBytes_ = nullptr
						):
				isCorrect(isCorrect_), message(message_), width(width_), height(height_), channels(channels_) {
					resultBytesLength = width * height * channels * sizeof(uint8_t); 
					resultBytes = resultBytes_;
				};
		};

		class Tracker{
			private:
				cv::Mat processResult;
			public:
				Tracker  () {};
				virtual ~Tracker() {};
				core::Response process(signed char bytes[], int witdh, int height);


		};
	 }
}
#endif
