#!/bin/bash
#install.sh

#IT HAS TO BE DEFINED ANDROID_NDK
export ANDROID_NDK="/home/carlos/Desktop/tools/android-ndk-r10b_x86";
JAVA_DIRECTORY="./wrappers";

if [ -z "$ANDROID_NDK" ]; then 
	echo "It is not defined ANDROID_NDK variable";
	exit 1;
fi

if [ -z "$1" ]
then 
	echo "Usage: `basename $0` [run|create_swig|compile_cpp|clean|setup-swig-cmake]";
	exit 1;
fi


clean() 
{
	echo "Cleaning installation...";
	rm -rf *.jar ./wrappers  *.so *.o CMakeFiles CMakeCache.txt cmake_install.cmake Makefile tracker_wrap.cpp libs obj	
	echo "Finished cleaning";
}
swig_fun()
{
	echo "Applying SWIG";
	mkdir ./wrappers;
	swig -c++ -java -package wrappers -outdir "${JAVA_DIRECTORY}" -o tracker_wrap.cpp tracker.i;
	echo "Finished skeleton creation";
}
compile_java()
{
	javac -target 1.7 ${JAVA_DIRECTORY}/*.java
	jar cf tracker_api.jar ${JAVA_DIRECTORY}/*.java ${JAVA_DIRECTORY}/*.class
	cp -rf tracker_api.jar ../libs/
	echo "Finished java compilation";
}
compile() {
	PATH="$PATH:$ANDROID_NDK";
	ndk-build
}

case `basename $1` in
	"run") swig_fun; compile_java; compile; rm -rf ${JAVA_DIRECTORY}/*.class *.so *.o CMakeFiles CMakeCache.txt cmake_install.cmake Makefile  obj;;
	"create_swig") swig_fun;;
	"compile_java") compile_java;;
	"compile_cpp") compile;;
	"clean") clean;;
	*) echo "Usage: `basename $0` [run|create_swig|compile_cpp|clean]"; exit 1;;
esac
