#ifndef _TIMER_H
#define _TIMER_H

#include <cstdio>
#include <string>
#include <ctime>
#include <chrono>



namespace cvc {
	namespace utils {
		class Timer
		{
		private:
			std::clock_t startTime;
		public:
			inline void start() {
				startTime = std::clock();
			}

			inline int stop() {
				return (int)((( std::clock() - startTime ) / (double) CLOCKS_PER_SEC)*1000);
			}
		};

		class Timer2
		{
		private:
			std::chrono::high_resolution_clock::time_point start;
			std::chrono::high_resolution_clock::time_point stop;
		public:
			inline void start2 () {
   				start = std::chrono::high_resolution_clock::now();
			};
			inline double stop2 () {
				stop = std::chrono::high_resolution_clock::now();
				return  std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();
			};
		};

	}

}

#endif



