%module tracker
%{
        #define SWIG_FILE_WITH_INIT
        #include "core.h"
%}
%include "arrays_java.i"
%include "std_string.i"

/* typemap for result structure */

%typemap(jni) const signed char *resultBytes "jbyteArray"
%typemap(jtype) const signed char *resultBytes "byte[]"
%typemap(jstype) const signed char *resultBytes "byte[]"
%typemap(javaout) const signed char *resultBytes {
        return $jnicall;
}

%typemap(out) const signed char * resultBytes {
        $result = JCALL1(NewByteArray, jenv, arg1->resultBytesLength);
        JCALL4(SetByteArrayRegion, jenv, $result, 0, arg1->resultBytesLength, $1);
}

%ignore resultBytesLength;


%pragma(java) jniclasscode=%{
        static {
                try {
                        System.loadLibrary("opencv_java");
                        System.loadLibrary("tracker");
                } catch (UnsatisfiedLinkError e) {
                        System.err.println("Native code library failed to load. \n" + e);
                        System.exit(1);
                }
        }
     %}


/* Let's just grab the original header file here */
%include "core.h"
